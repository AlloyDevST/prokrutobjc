//
//  ChooseUserDataDisplayManager.m
//  ProkrutObjc
//
//  Created by Tsyganov Stanislav on 14/01/16.
//  Copyright © 2016 DevAlloy. All rights reserved.
//

#import "ChooseUserDataDisplayManager.h"

#import "NimbusModels.h"

#import "Parse/Parse.h"

@interface ChooseUserDataDisplayManager ()

@property (nonatomic, strong) NITableViewActions *tableViewActions;
@property (nonatomic, strong) NITableViewModel *model;
@property (nonatomic, strong) NSArray *users;

@end

@implementation ChooseUserDataDisplayManager

- (instancetype)init {
    if (self = [super init]) {
        [self createActions];
        [self receiveUsers];
    }
    return self;
}

- (void)createActions {
    _tableViewActions = [[NITableViewActions alloc] initWithTarget:self];
    _tableViewActions.tableViewCellSelectionStyle = UITableViewCellSelectionStyleNone;
    __weak typeof(self)weakSelf = self;
    [self.tableViewActions attachToClass:[NITitleCellObject class]
                                tapBlock:^BOOL(NITitleCellObject *object, id target,
                                               NSIndexPath *indexPath) {
                                    __strong typeof(self)self = weakSelf;
                                    [self.ddmDelegate playerChoiceWasDone:[self.users objectAtIndex:indexPath.row]];
                                    return YES;
                                }];
}

- (void)receiveUsers {
    PFQuery *query = [PFUser query];
    __weak typeof(self)weakSelf = self;
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        __strong typeof(self)self = weakSelf;
        
        NSMutableArray *tableContents = [NSMutableArray new];
        for (PFUser *user in objects) {
            [tableContents addObject:[NITitleCellObject objectWithTitle:user.username]];
        }
        self.model = [[NITableViewModel alloc] initWithListArray:tableContents
                                                        delegate:(id)[NICellFactory class]];
        self.users = objects;
        [self.ddmDelegate needToUpdate];
    }];
}

- (id <UITableViewDataSource>)dataSource {
    return self.model;
}

- (id<UITableViewDelegate>)delegate {
    return self.tableViewActions;
}

@end
