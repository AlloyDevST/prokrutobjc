//
//  ChooseUserDataDisplayManager.h
//  ProkrutObjc
//
//  Created by Tsyganov Stanislav on 14/01/16.
//  Copyright © 2016 DevAlloy. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Parse/Parse.h"

@protocol ChooseUserDataDisplayManagerDelegate <NSObject>

- (void)needToUpdate;
- (void)playerChoiceWasDone:(PFUser *)user;

@end

@interface ChooseUserDataDisplayManager : NSObject

@property (nonatomic, weak) id<ChooseUserDataDisplayManagerDelegate> ddmDelegate;

- (id<UITableViewDataSource>)dataSource;
- (id<UITableViewDelegate>)delegate;

@end
