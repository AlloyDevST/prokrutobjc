//
//  StatisticsViewController.m
//  ProkrutObjc
//
//  Created by Tsyganov Stanislav on 10/01/16.
//  Copyright (c) 2016 DevAlloy. All rights reserved.
//


#import "StatisticsViewController.h"

#import "Parse/Parse.h"
#import "NimbusModels.h"

#import "UserStatisctics.h"

#import "UserStatiscticsCellObject.h"

static NSInteger const winPoints = 4;
static NSInteger const starPoints = 20;

@interface StatisticsViewController () <UITableViewDelegate>

@property (nonatomic, strong) NSArray *userStatistics;
@property (nonatomic, strong) NITableViewActions *tableViewActions;
@property (nonatomic, strong) NITableViewModel *model;

@end

@implementation StatisticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIRefreshControl *pullToRefreshControl = [UIRefreshControl new];
    [pullToRefreshControl addTarget:self action:@selector(didPullToRefesh:) forControlEvents:UIControlEventValueChanged];
    self.refreshControl = pullToRefreshControl;
    [self recieveMathcesData];
}

- (void)recieveMathcesData {
    PFQuery *query = [PFUser query];
    __weak typeof(self)weakSelf = self;
    [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
        if (objects && !error) {
            [self createUserStatisticsForUsers:objects];
            PFQuery *query = [PFQuery queryWithClassName:@"Match"];
            [query findObjectsInBackgroundWithBlock:^(NSArray * _Nullable objects, NSError * _Nullable error) {
                __strong typeof(self)self = weakSelf;
                [self updateStatisticsWithMatches:objects];
                [self createCellObjects];
                [self updateTableView];
                [self.refreshControl endRefreshing];
            }];
        }
    }];
}

- (void)didPullToRefesh:(UIRefreshControl *)refreshControl {
    [self recieveMathcesData];
}

#pragma mark - table view
- (void)createCellObjects {
    NSMutableArray *mutableModel = [NSMutableArray array];
    for (UserStatisctics *statistics in self.userStatistics) {
        UserStatiscticsCellObject *cellObject = [UserStatiscticsCellObject new];
        cellObject.userStatistics = statistics;
        [mutableModel addObject:cellObject];
    }
    self.model = [[NITableViewModel alloc] initWithListArray:mutableModel
                                                    delegate:(id)[NICellFactory class]];
    [self updateTableView];
}

- (void)updateTableView {
    self.tableView.delegate = [self.tableViewActions forwardingTo:self];
    self.tableView.dataSource = self.model;
    [self.tableView reloadData];
}

- (NITableViewActions *)tableViewActions {
    if (_tableViewActions == nil) {
        _tableViewActions = [[NITableViewActions alloc] initWithTarget:self];
    }
    return _tableViewActions;
}

#pragma mark - calculations
- (void)createUserStatisticsForUsers:(NSArray *)users {
    NSMutableArray *mutStatistics = [NSMutableArray new];
    for (PFUser *user in users) {
        UserStatisctics *userStatistics = [UserStatisctics new];
        userStatistics.user = user;
        [mutStatistics addObject:userStatistics];
    }
    self.userStatistics = mutStatistics;
}

- (void)updateStatisticsWithMatches:(NSArray *)matches {
    for (PFObject *match in matches) {
        [self calculateMatch:match];
    }
    
    for (UserStatisctics *userStatistics in self.userStatistics) {
        [self calculateCommonPropertiesForStatistics:userStatistics];
    }
}

- (void)calculateMatch:(PFObject *)match {
    NSInteger redScore = [match[@"redTeamScore"] integerValue];
    NSInteger blueScore = [match[@"blueTeamScore"] integerValue];
    
    PFUser *firstRedPlayer = match[@"firstRedPlayer"];
    UserStatisctics *firstRedPlayerStatistics = [self userStatisticsForUser:firstRedPlayer];
    [self calculateUserStatistics:firstRedPlayerStatistics
                   withAliasScore:redScore
                      andFoeScore:blueScore];
    
    PFUser *secondRedPlayer = match[@"secondRedPlayer"];
    UserStatisctics *secondRedPlayerStatistics = [self userStatisticsForUser:secondRedPlayer];
    [self calculateUserStatistics:secondRedPlayerStatistics
                   withAliasScore:redScore
                      andFoeScore:blueScore];

    PFUser *firstBluePlayer = match[@"firstBluePlayer"];
    UserStatisctics *firstBluePlayerStatistics = [self userStatisticsForUser:firstBluePlayer];
    [self calculateUserStatistics:firstBluePlayerStatistics
                   withAliasScore:blueScore
                      andFoeScore:redScore];
    
    PFUser *secondBluePlayer = match[@"secondBluePlayer"];
    UserStatisctics *secondBluePlayerStatistics = [self userStatisticsForUser:secondBluePlayer];
    [self calculateUserStatistics:secondBluePlayerStatistics
                   withAliasScore:blueScore
                      andFoeScore:redScore];

    
}

- (void)calculateUserStatistics:(UserStatisctics *)statistics
                 withAliasScore:(NSInteger)aliasScore
                    andFoeScore:(NSInteger)foeScore {
    statistics.points += aliasScore;
    if (aliasScore == 8) {
        statistics.wins += 1;
        statistics.points += winPoints;
        if (foeScore == 0) {
            statistics.stars += 1;
            statistics.points += starPoints;
        }
    } else {
        statistics.looses += 1;
        if (aliasScore == 0) {
            statistics.antiStars += 1;
        }
    }
}

- (void)calculateCommonPropertiesForStatistics:(UserStatisctics *)statistics {
    NSInteger matches = (statistics.wins + statistics.looses);
    if (matches == 0) {
        return;
    }
    statistics.winrate = (double)statistics.wins / matches;
    statistics.pointsPerMatch = (double)statistics.points / matches;
}

- (UserStatisctics *)userStatisticsForUser:(PFUser *)user {
    for (UserStatisctics *statistics in self.userStatistics) {
        if ([statistics.user.objectId isEqualToString:user.objectId]) {
            return statistics;
        }
    }
    NSLog(@"Не найдена статистика!");
    return nil;
}

#pragma mark - <UITableViewDelegate>

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 260.f;
}

@end