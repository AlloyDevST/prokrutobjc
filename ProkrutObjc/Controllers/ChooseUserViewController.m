//
//  ChooseUserViewController.m
//  ProkrutObjc
//
//  Created by Tsyganov Stanislav on 14/01/16.
//  Copyright © 2016 DevAlloy. All rights reserved.
//

#import "ChooseUserViewController.h"

#import "NimbusModels.h"

#import "ChooseUserDataDisplayManager.h"

@interface ChooseUserViewController () <ChooseUserDataDisplayManagerDelegate>

@property (nonatomic, strong) ChooseUserDataDisplayManager *choiceDDM;

@end

@implementation ChooseUserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.choiceDDM = [ChooseUserDataDisplayManager new];
    self.choiceDDM.ddmDelegate = self;
    self.tableView.dataSource = [self.choiceDDM dataSource];
    self.tableView.delegate = [self.choiceDDM delegate];
    
    [self.tableView reloadData];
}

- (void)playerChoiceWasDone:(PFUser *)user {
    self.completionBlock(user);
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)needToUpdate {
    self.tableView.dataSource = [self.choiceDDM dataSource];
    self.tableView.delegate = [self.choiceDDM delegate];
    [self.tableView reloadData];
}

@end
