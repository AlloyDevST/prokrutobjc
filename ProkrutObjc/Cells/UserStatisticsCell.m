//
//  UserStatisticsCell.m
//  ProkrutObjc
//
//  Created by Tsyganov Stanislav on 15/01/16.
//  Copyright © 2016 DevAlloy. All rights reserved.
//

#import "UserStatisticsCell.h"

#import "UserStatiscticsCellObject.h"

@implementation UserStatisticsCell

- (BOOL)shouldUpdateCellWithObject:(UserStatiscticsCellObject *)object {
    self.userNameTitle.text = object.userStatistics.user.username;
    self.winsLable.text = [@(object.userStatistics.wins) stringValue];
    self.losesLabel.text = [@(object.userStatistics.looses) stringValue];
    self.pointsLabel.text = [@(object.userStatistics.points) stringValue];
    
    double winrateInPercents = (double)object.userStatistics.winrate * 100;
    self.winrateLabel.text = [NSString stringWithFormat:@"%1.2f%%", winrateInPercents];
    self.pointsPerMatch.text = [NSString stringWithFormat:@"%1.2f", object.userStatistics.pointsPerMatch];
    self.starsLabel.text = [@(object.userStatistics.stars) stringValue];
    self.antiStarsLabel.text = [@(object.userStatistics.antiStars) stringValue];
    return YES;
}

@end
